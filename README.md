# Project 16-Program-za-dizajn-i-organizaciju-stana

Program sluzi za definisanje strukture stana, nakon toga se predefinisani predmeti rasporedjuju u prostoru.

![App Screenshot](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/16-program-za-dizajn-i-organizaciju-stana/-/raw/master/Video/App%20screenshot.PNG)

![App demo](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/16-program-za-dizajn-i-organizaciju-stana/-/raw/master/Video/smaller_gif.gif)

[Video demo](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/16-program-za-dizajn-i-organizaciju-stana/-/blob/master/Video/2021-03-21%2013-07-50.mkv)


## Aplikacija
Pokretanje kroz QtCreator.

## Developers

- [Matija Sreckovic, 167/2016](https://gitlab.com/m97s)
- [Pavle Velickovic, 67/2017](https://gitlab.com/PVelickovic)
- [Damjan Djoric, 293/2016](https://gitlab.com/dmn28)
- [Nevena Mesar, 107/2015](https://gitlab.com/nevena-m)
- [Anja Miletic, 97/2015](https://gitlab.com/anyamiletic)
