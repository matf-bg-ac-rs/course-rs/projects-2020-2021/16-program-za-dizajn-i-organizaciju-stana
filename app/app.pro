QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    elementmodel.cpp \
    elementutil.cpp \
    main.cpp \
    mainwindow.cpp \
    properties_component.cpp \
    rooms_component.cpp \
    scene.cpp \
    tabcontent.cpp

HEADERS += \
    constants.h \
    elementmodel.h \
    elementutil.h \
    mainwindow.h \
    properties_component.h \
    rooms_component.h \
    scene.h \
    tabcontent.h

FORMS += \
    mainwindow.ui \
    properties_component.ui \
    rooms_component.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc \
    resources.qrc \
    resources.qrc

DISTFILES += \
    img/icons/select.png \
    img/icons/wall.png
