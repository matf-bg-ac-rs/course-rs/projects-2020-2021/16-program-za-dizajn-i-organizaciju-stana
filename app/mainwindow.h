#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QSpacerItem>
#include <QList>
#include <QSharedPointer>
#include <QLabel>
#include "elementmodel.h"
#include "rooms_component.h"
#include "properties_component.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    properties_component *propertiesComponent;
    RoomsComponent *roomsComponent;

private:
    void initLists();
    void initVBox(QVBoxLayout *vBox, QList<QSharedPointer<ElementModel>> modelList);

    Ui::MainWindow *ui;
    //make one for other elems and group of templates
    QVBoxLayout *vboxKitchen;
    QList<QSharedPointer<ElementModel>> kitchenElemTemplates;

    QVBoxLayout *vboxBathroom;
    QList<QSharedPointer<ElementModel>> bathroomElemTemplates;

    QVBoxLayout *vboxBedroom;
    QList<QSharedPointer<ElementModel>> bedroomElemTemplates;

    QVBoxLayout *vboxLivingRoom;
    QList<QSharedPointer<ElementModel>> livingRoomElemTemplates;

    QVBoxLayout *vboxGeneral;
    QList<QSharedPointer<ElementModel>> generalElemTemplates;

public slots:
    void changeToolbarModeToSelect();
    void setWallDimensions(QString message);

public:
    QLabel *labelWallLength;

    QAction* createModeAction(QString text, Scene::Mode function, QString icon_path);
    QAction* createFuncAction(QString text, Scene::Functionality function, QString icon_path);
    void createActions();
    void createToolBar();

    QAction* dragAction;
    QAction* selectAction;
    QAction* lineAction;
    QActionGroup *modeActionGroup;
    QAction* screenshot;
    QAction* rotateLeft90;
    QAction* rotateLeft;
    QAction* rotateRight90;
    QAction* rotateRight;
    QAction* zoomIn;
    QAction* zoomOut;
    QAction* deleteSelectedEl;
    QToolBar* MainToolBar;
    QActionGroup *funcActionGroup;
};
#endif // MAINWINDOW_H
