#include "tabcontent.h"
#include "constants.h"
#include "tabcontent.h"
#include <QDebug>
#include <iostream>
#include <fstream>
#include <string.h>


TabContent::TabContent()
{
    scene = new Scene(this);
    view = new QGraphicsView(scene);
    view->setRenderHints(QPainter::Antialiasing);
    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    setCentralWidget(view);

    mySetSceneRect();

    view->fitInView(view->sceneRect(), Qt::KeepAspectRatio);

}

TabContent::~TabContent(){
    delete setSceneRectHelper1;
    delete setSceneRectHelper2;
    delete view;
    delete scene;
}

void TabContent::actionGroupClicked(QAction *action){
    scene->setMode(Scene::Mode(action->data().toInt()));
}

void TabContent::prepare3dOutput()
{
    std::ofstream output_file("input/input.txt");

    if (!output_file.is_open())
        return;

    foreach(auto item, scene->items()){
        QGraphicsLineItem* wall = dynamic_cast<QGraphicsLineItem*>( item );
        if(wall){
            output_file << outputWall(wall) << std::endl;
            continue;
        }
        ElementModel* el = dynamic_cast<ElementModel*>( item );
        if(el){
            output_file << outputElement(el) << std::endl;
            continue;
        }
    }

    output_file.close();
}

std::string TabContent::outputWall(QGraphicsLineItem* item){
    auto sep = OUTPUT_SEPARATOR.toStdString();
    return
        "Wall" + sep
       + QString::number(item->x()).toStdString() + sep
       + QString::number(item->y()).toStdString() + sep
       + "0" + sep
       + "250" + sep
       + QString::number(item->line().length()).toStdString() + sep
       + QString::number(item->rotation() + item->line().angle()).toStdString() + sep
       + "1.0" + sep;
}

std::string TabContent::outputElement(ElementModel* item){
    auto sep = OUTPUT_SEPARATOR.toStdString();
    return
        elementTypeToString(item->getElementType()).toStdString() + sep
        + QString::number(item->QGraphicsItem::x()).toStdString() + sep
        + QString::number(item->QGraphicsItem::y()).toStdString() + sep
        + QString::number(item->getElementWidth()).toStdString() + sep
        + QString::number(item->getElementDepth()).toStdString() + sep
        + QString::number(item->getElementHeight()).toStdString() + sep
        + QString::number(item->rotation()).toStdString() + sep
        + "1.0" + sep;
}

/*
 *  mySetSceneRect jer ako uradis setSceneRect, onda HandDrag vise ne funkcionise, zasto je tako ja nisam mogo da nadjem,
 *  i evo ovo je hack oko toga, dodas 2 nevidljive tacke koje ce da postave scenu
 */
void TabContent::mySetSceneRect()
{
    setSceneRectHelper1 = getSceneRectHelper(0, 0);
    setSceneRectHelper2 = getSceneRectHelper(INIT_ROOM_WIDTH, INIT_ROOM_HEIGHT);
    scene->addItem(setSceneRectHelper1);
    scene->addItem(setSceneRectHelper2);
}

QGraphicsEllipseItem *TabContent::getSceneRectHelper(int x, int y)
{
    QGraphicsEllipseItem* srh = new QGraphicsEllipseItem(x, y, 1, 1);
    srh->setBrush(Qt::green);
    srh->setVisible(false);
    return srh;
}

void TabContent::centerElementInView(ElementModel* el){
    QPointF center = view->mapToScene(rect().center());
    el->setPos(center - QPointF( el->height()/2, el->width()/2 ));
}


