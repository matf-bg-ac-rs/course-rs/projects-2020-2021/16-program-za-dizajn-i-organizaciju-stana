#include "rooms_component.h"
#include "ui_rooms_component.h"
#include "constants.h"

#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QLabel>
#include <QGraphicsEllipseItem>
#include <qgraphicsitem.h>
#include <QTimer>
#include <iostream>
#include <QDebug>
#include <QBitmap>
#include <QPixmap>
#include "elementutil.h"
#include <QFileInfo>
#include <QProcess>
#include <QDir>
#include <string>

RoomsComponent::RoomsComponent(QWidget *parent, properties_component* pk)
    : QWidget(parent)
    , pk(pk)
    , ui(new Ui::RoomsComponent)
    , defaultNewRoomNumber(1)
{
    ui->setupUi(this);
    initFirstTab();

}

RoomsComponent::~RoomsComponent()
{
    delete ui;
}

void RoomsComponent::initFirstTab()
{
    ui->Rooms->removeTab(getCurrTabIndex());
    addTabWithDefaultName();
    ui->Rooms->setCurrentIndex(0);
}


void RoomsComponent::connectElement(ElementModel *el){
    connect(el,   &ElementModel::elementSelected,
            pk,   &properties_component::incomingElementSlot,
            Qt::UniqueConnection);
}

void RoomsComponent::connectScene(Scene* scene){
    connect(scene,  &Scene::pasteElement,
            this,               &RoomsComponent::incomingElementSlot);
    connect(scene,  &Scene::wallLength,
            this,               &RoomsComponent::emitWallLengthHelperSignal);
    connect(scene,  &Scene::clearProperties,
            pk,                 &properties_component::nothingSelectedSlot);
}


TabContent* RoomsComponent::getCurrTabContent(){
    return tabContentByIndex[getCurrTabIndex()];
}

Scene* RoomsComponent::getCurrScene()
{
    return tabContentByIndex[getCurrTabIndex()]->scene;
}

int RoomsComponent::getNumOfTabs() const {
    return ui->Rooms->count();
}

int RoomsComponent::getCurrTabIndex(){
    return ui->Rooms->currentIndex();
}

bool RoomsComponent::isLastTab(int index) const {
    if (ui->Rooms->count() - 1 != index)
        return false;
    return true;
}


QString RoomsComponent::getDefaultTabName(){
    return QString("Room %1").arg(defaultNewRoomNumber++);
}

QString RoomsComponent::getValidTabNameFromPopUp(bool& isValidInput){

    auto defaultRoomName = getDefaultTabName();
    QString text = QInputDialog::getText(this, tr("Set Room Name"),
                                         tr("Room name:"), QLineEdit::Normal,
                                         defaultRoomName, &isValidInput,
                                         Qt::MSWindowsFixedSizeDialogHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint);
    if(!isValidInput){
        defaultNewRoomNumber--;
        return nullptr;
    }
    if(text.isEmpty()){
        return defaultRoomName;
    }
    if(text.compare(defaultRoomName) != 0){
        defaultNewRoomNumber--;
    }
    return text;
}

void RoomsComponent::addTab(QString tabName)
{
    int tabNumber = getNumOfTabs()-1;
    TabContent *tabContent = new TabContent();
    tabContentByIndex.push_back(tabContent);
    ui->Rooms->insertTab( tabNumber, tabContent, tabName);

    emit clearProperties();
    connectScene(tabContent->scene);
}

void RoomsComponent::addTabWithDefaultName()
{
    addTab(getDefaultTabName());
}

void RoomsComponent::addTabWithNameFromPopUp(){
    bool ok = true;
    QString tabName = getValidTabNameFromPopUp(ok);
    if(!ok){
        QTimer::singleShot(0.1, this, [this](){ ui->Rooms->setCurrentIndex(getCurrTabIndex()-1); });
        return;
    }
    addTab(tabName);
}


void RoomsComponent::on_Rooms_tabBarClicked(int index)
{
    if(!isLastTab(index)){
        return;
    }
    addTabWithNameFromPopUp();
}

void RoomsComponent::on_Rooms_tabBarDoubleClicked(int index)
{
    if(isLastTab(index)){
        return;
    }

    bool isValidInput = true;
    QString newName = getValidTabNameFromPopUp(isValidInput);
    if(!isValidInput)
        return;

    ui->Rooms->setTabText(index, newName);
}

void RoomsComponent::on_Rooms_currentChanged(int index)
{
    if(tabContentByIndex.isEmpty() || isLastTab(index))
        return;
    getCurrScene()->clearSelection();
    getCurrScene()->setMode(Scene::SelectObject);
    emit tabChanging();
}


void RoomsComponent::on_goTo3dBtn_clicked()
{
    goTo3D();
}

void RoomsComponent::goTo3D()
{
    QString resetPath = QDir::currentPath();
    QString basePath = QFileInfo(".").absolutePath();
    QString path = QDir::toNativeSeparators(basePath + "/app/3d-walkthrough");

    if(!QDir::setCurrent(path)){
        std::cout << "failed to set new path" << std::endl;
        return;
    }

    getCurrTabContent()->prepare3dOutput();

    QProcess process;
    process.setProgram("apartment");
    process.start();
    process.waitForFinished(-1);

    if(!QDir::setCurrent(resetPath))
        qDebug() << "close failed";
}


bool RoomsComponent::isRoomDeletionValid()
{
    if(isLastTab(getCurrTabIndex()))
        return false;

    auto answer = QMessageBox::question(this, "Delete room comfirmation",
                                        "Are you sure?",
                                        QMessageBox::Yes|QMessageBox::No);
    return (answer == QMessageBox::Yes);
}

void RoomsComponent::on_deleteRoomBtn_clicked()
{
    if (!isRoomDeletionValid())
        return;

    int tabToRemove = getCurrTabIndex();
    auto pageToDelete = ui->Rooms->widget(tabToRemove);

    ui->Rooms->removeTab(tabToRemove);
    delete pageToDelete;
    tabContentByIndex.remove(tabToRemove);

    if(getNumOfTabs() == 1){
        defaultNewRoomNumber = 1;
        addTabWithDefaultName();
    }

    QTimer::singleShot(0.1, this, [this](){ ui->Rooms->setCurrentIndex(0); });
}


void RoomsComponent::modeActionGroupClicked(QAction * action)
{
    getCurrScene()->setMode(Scene::Mode(action->data().toInt()));
}

void RoomsComponent::funcActionGroupClicked(QAction * action)
{
    auto functionality = Scene::Functionality(action->data().toInt());

    if(functionality == Scene::Functionality::RotateLeft)
        getCurrScene()->rotateElement(-ROTATE_PERCENTAGE);
    else if(functionality == Scene::Functionality::RotateRight)
        getCurrScene()->rotateElement(ROTATE_PERCENTAGE);
    else if(functionality == Scene::Functionality::RotateLeft90)
        getCurrScene()->rotateElement(-90);
    else if(functionality == Scene::Functionality::RotateRight90)
        getCurrScene()->rotateElement(90);
    else if(functionality == Scene::Functionality::DeleteSelectedEl)
        getCurrScene()->deleteSelectedElements();
    else if(functionality == Scene::Functionality::ZoomIn)
        tabContentByIndex[getCurrTabIndex()]->view->scale(1.1, 1.1);
    else if(functionality == Scene::Functionality::ZoomOut)
        tabContentByIndex[getCurrTabIndex()]->view->scale(0.9, 0.9);
    else if(functionality == Scene::Functionality::Screenshot)
        exportRoomPic();
    else
        std::cout << "Improbable failure: " << action->text().toStdString() << std::endl;
}


void RoomsComponent::exportRoomPic(){
    getCurrTabContent()->view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    getCurrTabContent()->view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::DirectoryOnly);
    QStringList fileNames;
    if(dialog.exec()){
        fileNames = dialog.selectedFiles();
        const QPixmap pixmap = getCurrTabContent()->grab();
        pixmap.save(getFileName(fileNames.at(0)));
    }

    getCurrTabContent()->view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    getCurrTabContent()->view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
}

QString RoomsComponent::getFileName(QString location){
    QString roomName = ui->Rooms->tabText(getCurrTabIndex());
    QString filename = "screenshot_"+roomName;
    int i = 0;
    if(QFileInfo::exists(location + QDir::separator() + filename+PIC_EXTENSION)){
        i = 1;
        while(QFileInfo::exists(location + QDir::separator() +filename+"_"+QString::number(i)+PIC_EXTENSION)){
            i++;
        }
        return location + QDir::separator() + filename+"_"+QString::number(i)+PIC_EXTENSION;
    }
    return location + QDir::separator() +filename+PIC_EXTENSION;
}


void RoomsComponent::incomingElementSlot(ElementModel *item, SceneInsertMode mode){
    ElementModel* newModel = ElementUtil::getTemplate(item->getElementType());
    connectElement(newModel);

    if(mode == pastedElement)
    {
        newModel->setPos(item->QGraphicsItem::pos() + PASTE_OFFSET);
        newModel->setSelected(true);
    }
    else if (mode == newElement)
    {
        getCurrTabContent()->centerElementInView(newModel);
        getCurrScene()->clearSelection();
        newModel->setSelected(true);
    }
    else
        qDebug() << "Unexpected behaviour";

    getCurrScene()->addElement(newModel);
}
