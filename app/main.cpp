#include "mainwindow.h"

#include <QApplication>
#include <QDebug>
#include <QStyleFactory>

int main(int argc, char *argv[])
{
    // global stylesheet file
    QCoreApplication::setAttribute(Qt::AA_UseStyleSheetPropagationInWidgetStyles, true);
    QFile File(":/style/stylesheet.qss");
    File.open(QFile::ReadOnly);
    QString StyleSheet = QLatin1String(File.readAll()); // ucitava ga kako treba
    File.close();

    QApplication::setDesktopSettingsAware(false);
    QApplication::setStyle("Fusion");

    QApplication a(argc, argv);
    a.setStyleSheet(StyleSheet);
    MainWindow w;
    w.show();
    return a.exec();
}
