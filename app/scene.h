#ifndef SCENE_H
#define SCENE_H

#include "elementmodel.h"
#include "properties_component.h"

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsLineItem>
#include <QAction>
#include <QGraphicsView>
#include <QKeyEvent>

class Scene : public QGraphicsScene
{
    Q_OBJECT
public:
    enum Mode {
        HandMode, SelectObject, DrawLine
    };
    enum Functionality {
        Screenshot,
        RotateLeft90, RotateLeft,
        RotateRight90, RotateRight,
        DeleteSelectedEl,
        ZoomIn, ZoomOut
    };
    Scene(QObject* parent = 0);
    void setMode(Mode mode);

signals:
    void clearProperties();
    void wallLength(QString lengthMessage);
    void pasteElement(ElementModel*, SceneInsertMode);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);

private:
    Mode sceneMode;
    QPointF origPoint;
    QGraphicsLineItem* wallElementToDraw;
    QList<QGraphicsItem*> sceneClipboard;
    void makeItemsControllable(bool areControllable);
    QGraphicsLineItem* wallCopyConstructor(QGraphicsLineItem* w);
    void pasteSceneItem(QGraphicsItem* item);

    void dummySelect();

public:
    void rotateElement(float degrees);
    void deleteSelectedElements();
    void addElement(QGraphicsItem* item);
};

#endif // SCENE_H
