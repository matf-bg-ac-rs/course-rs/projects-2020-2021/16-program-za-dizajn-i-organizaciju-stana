#include "properties_component.h"
#include "ui_properties_component.h"
#include "elementutil.h"
#include <QColorDialog>
#include <QString>
#include <iostream>

void properties_component::nothingSelectedSlot(){
    clearFields();
    disableFields(true);
    this->currentElement = nullptr;
}

void properties_component::disableFields(bool disable)
{
    this->ui->btn_reset_pixmap->setDisabled(disable);
    this->ui->checkBoxKeepRatio->setDisabled(disable);
    this->ui->le_position_x->setDisabled(disable);
    this->ui->le_position_y->setDisabled(disable);
    this->ui->le_size_width->setDisabled(disable);
    this->ui->le_size_height->setDisabled(disable);
    this->ui->le_size_depth->setDisabled(disable);
    this->ui->le_rotation->setDisabled(disable);
    this->ui->btn_color->setDisabled(disable);
    this->ui->le_name->setDisabled(disable);
}

void properties_component::clearFields()
{
    this->ui->checkBoxKeepRatio->setChecked(true);
    this->ui->le_position_x->clear();
    this->ui->le_position_y->clear();
    this->ui->le_size_width->clear();
    this->ui->le_size_height->clear();
    this->ui->le_size_depth->clear();
    this->ui->le_rotation->clear();
    this->ui->le_name->clear();
    this->ui->lbl_color_show->setStyleSheet("QLabel { background-color:white;}");
}

void properties_component::incomingElementSlot(ElementModel *item)
{
    disableFields(false);

    this->currentElement = item;

    /* init property fields */
    this->setPositionX(item->QGraphicsItem::pos().x());
    this->setPositionY(item->QGraphicsItem::pos().y());
    // height i width se odnose na dimenzije slike na prikazu, to je zapravo depth i width tim redosledom
    this->setSizeW(this->currentElement->getElementWidth());
    this->setSizeD(this->currentElement->getElementDepth());
    this->setSizeH(this->currentElement->getElementHeight());
    this->setRotation(item->rotation());
    this->setName(item->name);

    // color setup
    QString qstr_color = "";
    qstr_color.append("QLabel { background-color:");
    if(this->currentElement->color != NULL)
        qstr_color.append(this->currentElement->color->name());
    else
        qstr_color.append("white");
    qstr_color.append(";}");
    ui->lbl_color_show->setStyleSheet(qstr_color);
}

properties_component::properties_component(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::properties_component)
{
    ui->setupUi(this);
    ui->lbl_color_show->setStyleSheet("QLabel {background-color:white}");
    disableFields(true);
}

properties_component::~properties_component()
{
    delete ui;
}


void properties_component::on_le_position_x_editingFinished()
{
    QString value = ElementUtil::parseNumberInput(this->ui->le_position_x->text());
    if(value != ""){
        this->currentElement->setX(value.toDouble());
        this->setPositionX(value.toDouble());
    }
    else{
        this->setPositionX(this->currentElement->QGraphicsItem::pos().x());
    }
}

void properties_component::on_le_position_y_editingFinished()
{
    QString value = ElementUtil::parseNumberInput(this->ui->le_position_y->text());
    if(value != ""){
        this->currentElement->setY(value.toDouble());
        this->setPositionY(value.toDouble());
    }
    else{
        this->setPositionY(this->currentElement->QGraphicsItem::pos().y());
    }
}

void properties_component::on_le_size_width_editingFinished()
{
    QString value = ElementUtil::parseNumberInput(this->ui->le_size_width->text());
    bool keepRatio = ui->checkBoxKeepRatio->isChecked();
    this->currentElement->setElementWidth(value, keepRatio);
    this->setSizeW(this->currentElement->getElementWidth());
    this->setSizeD(this->currentElement->getElementDepth());
}

void properties_component::on_le_size_height_editingFinished()
{
    QString value = ElementUtil::parseNumberInput(this->ui->le_size_height->text());
    this->currentElement->setElementHeight(value);
    this->setSizeH(this->currentElement->getElementHeight());
}

void properties_component::on_le_size_depth_editingFinished()
{
    QString value = ElementUtil::parseNumberInput(this->ui->le_size_depth->text());
    bool keepRatio = ui->checkBoxKeepRatio->isChecked();
    this->currentElement->setElementDepth(value, keepRatio);
    this->setSizeD(this->currentElement->getElementDepth());
    this->setSizeW(this->currentElement->getElementWidth());
}

void properties_component::on_le_rotation_editingFinished()
{
    QString value = ElementUtil::parseNumberInput(this->ui->le_rotation->text());
    if(value != ""){
        double angle = this->ui->le_rotation->text().toDouble();
        while(angle > 360.0){
            angle -= 360.0;
        }
        while(angle < 0.0){
            angle += 360.0;
        }
        this->currentElement->setRotation(angle);
    }
    else{
        this->ui->le_rotation->setText(QString::number(this->currentElement->rotation()));
    }
}
void properties_component::on_le_name_editingFinished()
{
    this->currentElement->name = this->ui->le_name->text();
}

void properties_component::change_lbl_color_show(){

    QColorDialog dlg;
    QColor *newColor;

    if(this->currentElement->color != NULL)
        dlg.setCurrentColor( *this->currentElement->color );
    else
        dlg.setCurrentColor( QColor("white") );

    if ( dlg.exec() == QDialog::Accepted ){
         newColor = new QColor(dlg.currentColor());
    }else{
        QColor *oldColor = this->currentElement->color;
        if(oldColor == nullptr)
            newColor = new QColor("white");
        else
            newColor = oldColor;
    }

    QString color = newColor->name();
    QString qstr_color = "";
    qstr_color.append("QLabel { background-color:");
    qstr_color.append(color);
    qstr_color.append(";}");
    this->currentElement->applyColor(newColor);
    ui->lbl_color_show->setStyleSheet(qstr_color);
}

void properties_component::on_btn_color_clicked()
{
    change_lbl_color_show();
}

void properties_component::on_btn_reset_pixmap_clicked(){
    this->currentElement->reloadPixmap();
    setSizeW(this->currentElement->getElementWidth());
    setSizeD(this->currentElement->getElementDepth());
}


void properties_component::setPositionX(qreal x){
    this->ui->le_position_x->setText(QString::number(x));
}
void properties_component::setPositionY(qreal y){
    this->ui->le_position_y->setText(QString::number(y));
}
void properties_component::setSizeW(float width){
    this->ui->le_size_width->setText(QString::number(width));
}
void properties_component::setSizeH(float height){
    this->ui->le_size_height->setText(QString::number(height));
}
void properties_component::setSizeD(float depth){
    this->ui->le_size_depth->setText(QString::number(depth));
}
void properties_component::setRotation(float z){
    this->ui->le_rotation->setText(QString::number(z));
}
void properties_component::setName(QString name){
    this->ui->le_name->setText(name);
}
