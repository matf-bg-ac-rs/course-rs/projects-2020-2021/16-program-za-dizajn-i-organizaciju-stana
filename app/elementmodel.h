#ifndef ELEMENTMODEL_H
#define ELEMENTMODEL_H

#include <QObject>
#include <QVariant>
#include <QString>
#include <QColor>
#include <QGraphicsPixmapItem>
#include <QStandardItemModel>
#include <QOpenGLTexture>
#include <QToolButton>
#include <QPixmap>
#include <QIcon>
#include "constants.h"
#include <iostream>
#include <QRectF>
#include <QPointF>
#include <QGraphicsScene>
#include <QGraphicsItem>

class ElementModel :
        public QToolButton,
        public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    ElementModel(const ElementModel *model);
    ElementModel(QString picLocation, QString name, ElementType elementType);
    ElementModel(QString name, ElementType type);
    ~ElementModel();

    ElementType getElementType();

    void dummySelection();
signals:
    void elementSelected(ElementModel*, SceneInsertMode);

public slots:
    void emitSelectedElement(){
        emit elementSelected(this, newElement);
    }
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);


public:
    QColor *color;
    QString name;
    QString description;
    QOpenGLTexture *texture;
    QString picLocation;

    void applyColor(QColor *newColor);
    void reloadPixmap();
    void setElementHeight(QString value);
    int getElementHeight();
    void setElementWidth(QString value, bool keepRatio);
    int getElementWidth();
    void setElementDepth(QString value, bool keepRatio); // height 2d pixmapa
    int getElementDepth(); // height 2d pixmapa

private:
    ElementType elementType;
    int elementHeight;
    int elementWidth;
    int elementDepth;
    void initDefaultValues();
};

#endif // ELEMENTMODEL_H
