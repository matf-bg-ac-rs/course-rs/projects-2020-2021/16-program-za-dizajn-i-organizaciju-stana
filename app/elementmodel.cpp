#include "elementmodel.h"
#include "constants.h"
#include "elementutil.h"
#include <QPixmap>
#include <QImage>
#include <QDebug>
#include <QRgb>


void ElementModel::applyColor(QColor* newColor){
    QRgb oldRgba = this->color->rgba();
    QImage tmp = this->pixmap().toImage();
    int oldr = qRed(oldRgba);
    int oldg = qGreen(oldRgba);
    int oldb = qBlue(oldRgba);

    for(int y = 0; y < tmp.height(); y++){
        for(int x= 0; x < tmp.width(); x++){
            QRgb rgba = tmp.pixelColor(x,y).rgba();
            int r = qRed(rgba);
            int g = qGreen(rgba);
            int b = qBlue(rgba);

            if(oldb == b && oldg == g && oldr == r){
                newColor->setAlpha(tmp.pixelColor(x,y).alpha());
                tmp.setPixelColor(x,y,*newColor);
            }
        }
    }
    this->setPixmap(QPixmap::fromImage(tmp));
    this->color = newColor;
}

void ElementModel::reloadPixmap(){
    this->setPixmap(QPixmap::fromImage(QImage(this->picLocation)));
    this->elementWidth =this->pixmap().width();
    this->elementDepth = this->pixmap().height();
    QColor* originalColor = new QColor(this->color->name());
    this->color->setNamedColor("white");
    this->applyColor(originalColor);
}

void ElementModel::setElementHeight(QString value){
    this->elementHeight = ElementUtil::checkSizeValue(value);
}

int ElementModel::getElementHeight(){
    return this->elementHeight;
}

void ElementModel::setElementWidth(QString value, bool keepRatio){
    int newValue = ElementUtil::checkSizeValue(value);
    if(newValue != 0){
        if(elementType == ElementType::wall){ // ovo je simple element njemu su dozvoljene razne dimenzije ili makar neko drugo ogranicenje
            this->elementWidth = newValue;
            return;
        }
        if(keepRatio){
            // ako je druga vrednost manja od dozvoljene svesti na skaliranje na nju tako da ona bude minimalna,
            // onda ce ova druga sigurno biti u granicama, pod pretpostavkom da se inicijalne pixmap dimenzije uklapaju u tu pricu
            float otherNewValue = this->pixmap().height() * newValue * (1.0 / this->pixmap().width());
            if(otherNewValue < MIN_SIZE){
                this->setPixmap(this->pixmap().scaledToHeight(MIN_SIZE, Qt::SmoothTransformation));
            }
            else if(otherNewValue > MAX_SIZE){
                this->setPixmap(this->pixmap().scaledToHeight(MAX_SIZE, Qt::SmoothTransformation));
            }
            else{
                this->setPixmap(this->pixmap().scaledToWidth(newValue, Qt::SmoothTransformation));
            }
        }
        else{
            this->setPixmap(this->pixmap().scaled(newValue,this->elementDepth, Qt::IgnoreAspectRatio));
        }
        this->elementWidth = this->pixmap().width();
        this->elementDepth = this->pixmap().height();
    }
}

int ElementModel::getElementWidth(){
    return this->elementWidth;
}

void ElementModel::setElementDepth(QString value, bool keepRatio){
    int newValue = ElementUtil::checkSizeValue(value);
    if(newValue != 0){
        if(keepRatio){
            // ako je druga vrednost manja od dozvoljene svesti na skaliranje na nju tako da ona bude minimalna,
            // onda ce ova druga sigurno biti u granicama, pod pretpostavkom da se inicijalne pixmap dimenzije uklapaju u tu pricu
            float otherNewValue = this->pixmap().width() * newValue * (1.0 / this->pixmap().height());
            if(otherNewValue < MIN_SIZE){
                this->setPixmap(this->pixmap().scaledToWidth(MIN_SIZE, Qt::SmoothTransformation));
            }
            else if(otherNewValue > MAX_SIZE){
                this->setPixmap(this->pixmap().scaledToWidth(MAX_SIZE, Qt::SmoothTransformation));
            }
            else{
                this->setPixmap(this->pixmap().scaledToHeight(newValue, Qt::SmoothTransformation));
            }
        }
        else{
            this->setPixmap(this->pixmap().scaled(this->elementWidth,newValue, Qt::IgnoreAspectRatio));
        }
        this->elementDepth = this->pixmap().height();
        this->elementWidth = this->pixmap().width();
    }
}

int ElementModel::getElementDepth(){
    return this->elementDepth;
}

void ElementModel::initDefaultValues(){
    connect(this, SIGNAL(clicked()), this, SLOT(emitSelectedElement()));

    this->setPixmap(QPixmap::fromImage(QImage(this->picLocation)));
    this->elementHeight = INIT_HEIGHT;             // za 3d
    this->elementWidth= this->pixmap().width();
    this->elementDepth = this->pixmap().height();  // posto je 2d
    this->color = new QColor("white");
    this->setText(this->name);
    this->setGeometry(1,1,10,10);
    this->setMinimumSize(125,125);
    this->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    this->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    this->setTransformOriginPoint(this->boundingRect().center());
    this->setFlag(QGraphicsItem::ItemIsSelectable, true);
    this->setFlag(QGraphicsItem::ItemIsMovable, true);
    this->setPos(0, 0);
    setIcon(QIcon(this->picLocation));
    setIconSize(QSize(100,100));
    setAutoFillBackground(true);
}

ElementModel::ElementModel(const ElementModel *model)
{
    this->picLocation = model->picLocation;
    this->name = model->name;
    this->elementType = model->elementType;
    initDefaultValues();
}

ElementModel::ElementModel(QString picLocation, QString name, ElementType elementType)
{
    this->picLocation = picLocation;
    this->elementType = elementType;
    this->name = name;
    initDefaultValues();
}

ElementModel::ElementModel(QString name, ElementType type){
    std::cout<< "tip " << this->elementType << std::endl;
    this->name = name;
    this->elementType = type;
}

ElementType ElementModel::getElementType(){
    return this->elementType;
}

void ElementModel::dummySelection(){
    emit clicked();
}

void ElementModel::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    emit clicked();
    QGraphicsItem::mouseMoveEvent(event);
}

ElementModel::~ElementModel()
{
    // todo: free mem
}
