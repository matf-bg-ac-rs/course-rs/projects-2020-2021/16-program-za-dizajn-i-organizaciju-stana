#include "elementutil.h"
#include "constants.h"
#include <QRegExp>
#include <QString>
#include <QStringList>
#include <QVector>

QString ElementUtil::parseNumberInput(QString aInput){
    return aInput.replace(QRegExp("[^-+0-9.]*"), "");
}

int ElementUtil::checkSizeValue(QString value){
    int intValue = value.toInt();
    if(value != "" && intValue > 0){
        // todo: implementacija ogranicenja tako da ne puca odrediti minimum sirinu/visinu
        // tako da ni jedna vrednost ne predje min/max
        if(intValue < MIN_SIZE)
            return MIN_SIZE;
        else if(intValue > MAX_SIZE)
            return MAX_SIZE;
        else
            return intValue;
    }
    return 0;
}

ElementModel* ElementUtil::getTemplate(ElementType type){
    switch(type){
    case lowerCabinetDouble: return getTemplateLowerCabinetDouble();
    case upperCabinetSingle: return getTemplateUpperCabinetSingle();
    case upperCabinetDouble: return getTemplateUpperCabinetDouble();
    case fridge: return getTemplateFridge();
    case dishwasher: return getTemplateDishwasher();
    case microwave: return getTemplateMicrowave();
    case aspirator: return getTemplateAspirator();
    case doubleSink: return getTemplateDoubleSink();
    case singleSink: return getTemplateSingleSink();
    case sinkDrain: return getTemplateSinkDrain();

    case bathtub: return getTemplateBathtub();
    case cornerBathtub: return getTemplateCornerBathtub();
    case bidet: return getTemplateBidet();
    case handbasin: return getTemplateHandbasin();
    case cornerHandbasin: return getTemplateCornerHandbasin();
    case roundHandbasin: return getTemplateRoundHandbasin();
    case showerhead: return getTemplateShowerhead();
    case cornerTub: return getTemplateCornerTub();
    case roundTub: return getTemplateRoundTub();
    case squareTub: return getTemplateSquareTub();

    case doubleBed: return getTemplateDoubleBed();
    case singleBed: return getTemplateSingleBed();
    case doubleCloset: return getTemplateDoubleCloset();
    case oneSideCloset: return getTemplateOneSideCloset();

    case armchair: return getTemplateArmchair();
    case officeChair: return getTemplateOfficeChair();
    case officeDesk: return getTemplateOfficeDesk();
    case cornerSofa: return getTemplateCornerSofa();
    case cornerDoubleSofa: return getTemplateCornerDoubleSofa();
    case doubleSofa: return getTemplateDoubleSofa();
    case longSofa: return getTemplateLongSofa();
    case triSofa: return getTemplateTriSofa();

    case aircond: return getTemplateAircond();
    case chair: return getTemplateChair();
    case drawer: return getTemplateDrawer();
    case circle: return getTemplateCircle();
    case elipse: return getTemplateElipse();
    case door: return getTemplateDoor();
    case windowElement: return getTemplateWindowElement();

    default: return getTemplateCircle();
    }
}

ElementModel* ElementUtil::getTemplateLowerCabinetDouble(){
    return new ElementModel(":/img/img/cabinet_low_double.png", "Lower cabinet double", lowerCabinetDouble);
}
ElementModel* ElementUtil::getTemplateUpperCabinetSingle(){
    return new ElementModel(":/img/img/cabinet_up_single.png", "Upper cabinet single", upperCabinetSingle);
}
ElementModel* ElementUtil::getTemplateUpperCabinetDouble(){
    return new ElementModel(":/img/img/cabinet_up_double.png", "Upper cabinet double", upperCabinetDouble);
}
ElementModel* ElementUtil::getTemplateFridge(){
    return new ElementModel(":/img/img/fridge.png", "Fridge", fridge);
}
ElementModel* ElementUtil::getTemplateDishwasher(){
    return new ElementModel(":/img/img/dishwasher.png", "Dishwasher", dishwasher);
}
ElementModel* ElementUtil::getTemplateMicrowave(){
    return new ElementModel(":/img/img/microwave.png", "Microwave", microwave);
}
ElementModel* ElementUtil::getTemplateAspirator(){
    return new ElementModel(":/img/img/aspirator.png", "Aspirator", aspirator);
}
ElementModel* ElementUtil::getTemplateDoubleSink(){
    return new ElementModel(":/img/img/sink_double.png", "Double sink", doubleSink);
}
ElementModel* ElementUtil::getTemplateSingleSink(){
    return new ElementModel(":/img/img/sink_single.png", "Single sink", singleSink);
}
ElementModel* ElementUtil::getTemplateSinkDrain(){
    return new ElementModel(":/img/img/sink_drain.png", "Sink drain", sinkDrain);
}

ElementModel* ElementUtil::getTemplateBathtub(){
    return new ElementModel(":/img/img/bathtub.png", "Bathtub", bathtub);
}
ElementModel* ElementUtil::getTemplateCornerBathtub(){
    return new ElementModel(":/img/img/bathtub_corner.png", "Corner bathtub", cornerBathtub);
}
ElementModel* ElementUtil::getTemplateBidet(){
    return new ElementModel(":/img/img/bidet.png", "Bidet", bidet);
}
ElementModel* ElementUtil::getTemplateHandbasin(){
    return new ElementModel(":/img/img/handbasin.png", "Handbasin", handbasin);
}
ElementModel* ElementUtil::getTemplateCornerHandbasin(){
    return new ElementModel(":/img/img/handbasin_corner.png", "Corner handbasin", cornerHandbasin);
}
ElementModel* ElementUtil::getTemplateRoundHandbasin(){
    return new ElementModel(":/img/img/handbasin_round.png", "Round handbasin", roundHandbasin);
}
ElementModel* ElementUtil::getTemplateShowerhead(){
    return new ElementModel(":/img/img/showerhead.png", "Showerhead", showerhead);
}
ElementModel* ElementUtil::getTemplateCornerTub(){
    return new ElementModel(":/img/img/tub_corner.png", "Corner tub", cornerTub);
}
ElementModel* ElementUtil::getTemplateRoundTub(){
    return new ElementModel(":/img/img/tub_round.png", "Round tub", roundTub);
}
ElementModel* ElementUtil::getTemplateSquareTub(){
    return new ElementModel(":/img/img/tub_square.png", "Square tub", squareTub);
}

ElementModel* ElementUtil::getTemplateDoubleBed(){
    return new ElementModel(":/img/img/bed_double.png", "Double bed", doubleBed);
}
ElementModel* ElementUtil::getTemplateSingleBed(){
    return new ElementModel(":/img/img/bed_single.png", "Single bed", singleBed);
}
ElementModel* ElementUtil::getTemplateDoubleCloset(){
    return new ElementModel(":/img/img/closet_double.png", "Double closet", doubleCloset);
}
ElementModel* ElementUtil::getTemplateOneSideCloset(){
    return new ElementModel(":/img/img/closet_one_side.png", "One side closet", oneSideCloset);
}

ElementModel* ElementUtil::getTemplateArmchair(){
    return new ElementModel(":/img/img/armchair.png", "Armchair", armchair);
}
ElementModel* ElementUtil::getTemplateOfficeChair(){
    return new ElementModel(":/img/img/office_chair.png", "Office chair", officeChair);
}
ElementModel* ElementUtil::getTemplateOfficeDesk(){
    return new ElementModel(":/img/img/office_desk.png", "Office desk", officeDesk);
}
ElementModel* ElementUtil::getTemplateCornerSofa(){
    return new ElementModel(":/img/img/sofa_corner.png", "Corner sofa", cornerSofa);
}
ElementModel* ElementUtil::getTemplateCornerDoubleSofa(){
    return new ElementModel(":/img/img/sofa_corner_double.png", "Corner double sofa", cornerDoubleSofa);
}
ElementModel* ElementUtil::getTemplateDoubleSofa(){
    return new ElementModel(":/img/img/sofa_double.png", "Double sofa", doubleSofa);
}
ElementModel* ElementUtil::getTemplateLongSofa(){
    return new ElementModel(":/img/img/sofa_long.png", "Long sofa", longSofa);
}
ElementModel* ElementUtil::getTemplateTriSofa(){
    return new ElementModel(":/img/img/sofa_tri.png", "Tri sofa", triSofa);
}

ElementModel* ElementUtil::getTemplateAircond(){
    return new ElementModel(":/img/img/aircond.png", "Aircond", aircond);
}
ElementModel* ElementUtil::getTemplateChair(){
    return new ElementModel(":/img/img/chair.png", "Chair", chair);
}
ElementModel* ElementUtil::getTemplateDrawer(){
    return new ElementModel(":/img/img/drawer.png", "Drawer", drawer);
}
ElementModel* ElementUtil::getTemplateCircle(){
    return new ElementModel(":/img/img/circle_trashbin_other.png", "Circle/Trashbin", circle);
}
ElementModel* ElementUtil::getTemplateElipse(){
    return new ElementModel(":/img/img/elipse_table_rug.png", "Elipse/Table/Rug", elipse);
}

ElementModel* ElementUtil::getTemplateDoor(){
    return new ElementModel(":/img/img/door.png", "Door", door);
}
ElementModel* ElementUtil::getTemplateWindowElement(){
    return new ElementModel(":/img/img/window.png", "Window", windowElement);
}
