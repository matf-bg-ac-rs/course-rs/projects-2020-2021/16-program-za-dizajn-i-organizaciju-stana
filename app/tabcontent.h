#ifndef TABCONTENT_H
#define TABCONTENT_H

#include <QMainWindow>
#include <QGraphicsView>
#include <QToolBar>
#include "properties_component.h"
#include "scene.h"
#include <QAction>

class TabContent : public QMainWindow
{
    Q_OBJECT
public:
    TabContent();
    ~TabContent();
    void actionGroupClicked(QAction*);
    void prepare3dOutput();

private:
    void mySetSceneRect();
    QGraphicsEllipseItem* getSceneRectHelper(int x, int y);


    std::string outputWall(QGraphicsLineItem* item);
    std::string outputElement(ElementModel *item);
public:
    QGraphicsView* view;
    Scene* scene;
    QVector<ElementModel*> sceneElements;
    QGraphicsEllipseItem* setSceneRectHelper1;
    QGraphicsEllipseItem* setSceneRectHelper2;
    void centerElementInView(ElementModel* el);
};

#endif
