#ifndef ELEMENTUTIL_H
#define ELEMENTUTIL_H

#include "elementmodel.h"
#include <QString>
#include <QPair>
#include <tuple>

namespace ElementUtil
{
ElementModel* getTemplate(ElementType type);

ElementModel* getTemplateLowerCabinetDouble();
ElementModel* getTemplateUpperCabinetSingle();
ElementModel* getTemplateUpperCabinetDouble();
ElementModel* getTemplateFridge();
ElementModel* getTemplateDishwasher();
ElementModel* getTemplateMicrowave();
ElementModel* getTemplateAspirator();
ElementModel* getTemplateDoubleSink();
ElementModel* getTemplateSingleSink();
ElementModel* getTemplateSinkDrain();

ElementModel* getTemplateBathtub();
ElementModel* getTemplateCornerBathtub();
ElementModel* getTemplateBidet();
ElementModel* getTemplateHandbasin();
ElementModel* getTemplateCornerHandbasin();
ElementModel* getTemplateRoundHandbasin();
ElementModel* getTemplateShowerhead();
ElementModel* getTemplateCornerTub();
ElementModel* getTemplateRoundTub();
ElementModel* getTemplateSquareTub();

ElementModel* getTemplateDoubleBed();
ElementModel* getTemplateSingleBed();
ElementModel* getTemplateDoubleCloset();
ElementModel* getTemplateOneSideCloset();

ElementModel* getTemplateArmchair();
ElementModel* getTemplateOfficeChair();
ElementModel* getTemplateOfficeDesk();
ElementModel* getTemplateCornerSofa();
ElementModel* getTemplateCornerDoubleSofa();
ElementModel* getTemplateDoubleSofa();
ElementModel* getTemplateLongSofa();
ElementModel* getTemplateTriSofa();

ElementModel* getTemplateAircond();
ElementModel* getTemplateChair();
ElementModel* getTemplateDrawer();
ElementModel* getTemplateCircle();
ElementModel* getTemplateElipse();
ElementModel* getTemplateDoor();
ElementModel* getTemplateWindowElement();

QString parseNumberInput(QString aInput);
int checkSizeValue(QString value);
}

#endif // ELEMENTUTIL_H
