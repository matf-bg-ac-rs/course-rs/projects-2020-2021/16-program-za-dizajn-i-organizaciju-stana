# integrate scene with rest of app

`main.cpp` must contain `#define STB_IMAGE_IMPLEMENTATION` as the first line. 
Must include `#include "Scene.h"` in the include lines.
In order to start a scene, trigger the following code: 
`Scene scene = Scene();
	scene.launchScene();`
Without custom elements for now (a 'random' scene will appear). Exit the window by pressing esc key.