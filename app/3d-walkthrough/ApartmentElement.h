#pragma once
#include <string>

class ApartmentElement
{
public:
	ApartmentElement();
	ApartmentElement(std::string el_type, 
		double x_coord, 
		double y_coord, 
		double el_width,
		double el_length,
		double el_height,
		double rot,
		double el_scale);

	std::string get_element_type() { return element_type; }
	double get_x() { return x; }
	double get_y() { return y; }
	double get_width() { return width; }
	double get_length() { return length; }
	double get_height() { return height; }
	double get_rotation() { return rotation; }
	double get_scale() { return scale; }

	std::string showElement();

	~ApartmentElement();

private:
	std::string element_type;
	double x;
	double y;
	double width;
	double length;
	double height;
	double rotation;
	double scale;
};

