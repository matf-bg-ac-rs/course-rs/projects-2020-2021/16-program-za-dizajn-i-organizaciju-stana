#include "Scene.h"

const float toRadians = 3.14159265f / 180.0f;

Window mainWindow;
std::vector<Mesh*> meshList;
std::vector<Shader> shaderList;
Camera camera;

Texture brickTexture;
Texture dirtTexture;
Texture floorTexture;

Material shinyMaterial;
Material dullMaterial;

// just need one of each
Model chair;
Model door;
Model dresser;
Model wall;
Model room;
Model plant;
Model table;
Model sofa;

DirectionalLight mainLight;
// todo get number from input
PointLight pointLights[MAX_POINT_LIGHTS];

ApartmentElement ap_el;

GLfloat deltaTime = 0.0f;
GLfloat lastTime = 0.0f;

// Vertex Shader
static const char* vShader = "Shaders/shader.vert";

// Fragment Shader
static const char* fShader = "Shaders/shader.frag";

std::vector<ApartmentElement> readFile(const char* filePath) {
	std::ifstream ifile(filePath, std::ios::in);
	std::vector<ApartmentElement> elements;

	//check to see that the file was opened correctly:
	if (!ifile.is_open()) {
		std::cerr << "There was a problem opening the input file!\n";
		exit(1);//exit or do additional error checking
	}

	std::string element_type;
	double x,
		y,
		width,
		length,
		height,
		rotation,
		scale;

	//keep storing values from the text file so long as data exists:
	while (ifile >> element_type >> x >> y >> width >> length >> height >> rotation >> scale) {
		ap_el = ApartmentElement(element_type, x, y, width, length, height, rotation, scale);
		elements.push_back(ap_el);
	}

	//verify that the scores were stored correctly:
	for (int i = 0; i < elements.size(); ++i) {
		std::cout << "element: " << elements[i].showElement() << std::endl;
	}

	return elements;
}

void calcAverageNormals(unsigned int * indices, unsigned int indiceCount, GLfloat * vertices, unsigned int verticeCount,
	unsigned int vLength, unsigned int normalOffset)
{
	for (size_t i = 0; i < indiceCount; i += 3)
	{
		unsigned int in0 = indices[i] * vLength;
		unsigned int in1 = indices[i + 1] * vLength;
		unsigned int in2 = indices[i + 2] * vLength;
		glm::vec3 v1(vertices[in1] - vertices[in0], vertices[in1 + 1] - vertices[in0 + 1], vertices[in1 + 2] - vertices[in0 + 2]);
		glm::vec3 v2(vertices[in2] - vertices[in0], vertices[in2 + 1] - vertices[in0 + 1], vertices[in2 + 2] - vertices[in0 + 2]);
		glm::vec3 normal = glm::cross(v1, v2);
		normal = glm::normalize(normal);

		in0 += normalOffset; in1 += normalOffset; in2 += normalOffset;
		vertices[in0] += normal.x; vertices[in0 + 1] += normal.y; vertices[in0 + 2] += normal.z;
		vertices[in1] += normal.x; vertices[in1 + 1] += normal.y; vertices[in1 + 2] += normal.z;
		vertices[in2] += normal.x; vertices[in2 + 1] += normal.y; vertices[in2 + 2] += normal.z;
	}

	for (size_t i = 0; i < verticeCount / vLength; i++)
	{
		unsigned int nOffset = i * vLength + normalOffset;
		glm::vec3 vec(vertices[nOffset], vertices[nOffset + 1], vertices[nOffset + 2]);
		vec = glm::normalize(vec);
		vertices[nOffset] = vec.x; vertices[nOffset + 1] = vec.y; vertices[nOffset + 2] = vec.z;
	}
}

void CreateFloor()
{

	unsigned int floorIndices[] = {
		0, 2, 1,
		1, 2, 3
	};

	GLfloat floorVertices[] = {
		-1.5f, 0.0f, -1.5f,	0.0f, 0.0f,		0.0f, -1.5f, 0.0f,
		1.5f, 0.0f, -1.5f,	1.5f, 0.0f,	0.0f, -1.5f, 0.0f,
		-1.5f, 0.0f, 1.5f,	0.0f, 1.5f,	0.0f, -1.5f, 0.0f,
		1.5f, 0.0f, 1.5f,		1.5f, 1.5f,	0.0f, -1.5f, 0.0f
	};

	Mesh *obj3 = new Mesh();
	obj3->CreateMesh(floorVertices, floorIndices, 32, 6);
	meshList.push_back(obj3);
}

void CreateObjects()
{
	unsigned int indices[] = {
		0, 3, 1,
		1, 3, 2,
		2, 3, 0,
		0, 1, 2
	};

	GLfloat vertices[] = {
		//	x      y      z			u	  v			nx	  ny    nz
			-1.0f, -1.0f, 0.0f,		0.0f, 0.0f,		0.0f, 0.0f, 0.0f,
			0.0f, -1.0f, 1.0f,		0.5f, 0.0f,		0.0f, 0.0f, 0.0f,
			1.0f, -1.0f, 0.0f,		1.0f, 0.0f,		0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f,		0.5f, 1.0f,		0.0f, 0.0f, 0.0f
	};

	calcAverageNormals(indices, 12, vertices, 32, 8, 5);

	Mesh *obj1 = new Mesh();
	obj1->CreateMesh(vertices, indices, 32, 12);
	meshList.push_back(obj1);

	Mesh *obj2 = new Mesh();
	obj2->CreateMesh(vertices, indices, 32, 12);
	meshList.push_back(obj2);
}

void CreateShaders()
{
	Shader *shader1 = new Shader();
	shader1->CreateFromFiles(vShader, fShader);
	shaderList.push_back(*shader1);
}

void launchSceneInner()
{
	mainWindow = Window(1024, 768);
	mainWindow.Initialise();

	CreateObjects();
	CreateFloor();
	CreateShaders();

	camera = Camera(glm::vec3(1.0f, 3.0f, 2.5f), glm::vec3(0.0f, 1.0f, 0.0f), -90.0f, -60.0f, 5.0f, 0.5f);

	brickTexture = Texture("Textures/brick.png");
	brickTexture.LoadTextureA();
	dirtTexture = Texture("Textures/dirt.png");
	dirtTexture.LoadTextureA();
	floorTexture = Texture("Textures/wood_floor.png");
	floorTexture.LoadTextureA();

	// intensity usually a factor of 2; 32 is a normal to high intensity
	shinyMaterial = Material(1.0f, 32);
	dullMaterial = Material(0.3f, 4);

	// define models
	door = Model();
	door.LoadModel("Models/Door.3ds");
	dresser = Model();
	dresser.LoadModel("Models/Dresser.obj");
	wall = Model();
	wall.LoadModel("Models/wall.obj");
	plant = Model();
	plant.LoadModel("Models/plant.obj");
	table = Model();
	table.LoadModel("Models/Wood_Table.obj");
	chair = Model();
	chair.LoadModel("Models/chair.obj");
	sofa = Model();
	sofa.LoadModel("Models/sofa.obj");

	// ambient / diffuse
	mainLight = DirectionalLight(1.0f, 1.0f, 1.0f,
		0.2f, 0.5f,
		2.0f, -1.0f, -2.0f);

	unsigned int pointLightCount = 0;
	// green hue point light
	pointLights[0] = PointLight(0.0f, 0.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f, 0.0f,
		0.3f, 0.2f, 0.1f);
	pointLightCount++;
	// blue hue point light
	pointLights[1] = PointLight(0.0f, 1.0f, 0.0f,
		0.0f, 1.0f,
		-4.0f, 2.0f, 0.0f,
		0.3f, 0.1f, 0.1f);
	pointLightCount++;

	// init variables used in shaders
	GLuint uniformProjection = 0, uniformModel = 0, uniformView = 0, uniformEyePosition = 0,
		uniformSpecularIntensity = 0, uniformShininess = 0;
	glm::mat4 projection = glm::perspective(glm::radians(45.0f), (GLfloat)mainWindow.getBufferWidth() / mainWindow.getBufferHeight(), 0.1f, 100.0f);

	std::vector<ApartmentElement> elements;
	elements = readFile("input/input.txt");

	// Loop until window closed
	while (!mainWindow.getShouldClose())
	{
		GLfloat now = glfwGetTime();
		deltaTime = now - lastTime;
		lastTime = now;

		// Get + Handle User Input
		glfwPollEvents();

		camera.keyControl(mainWindow.getsKeys(), deltaTime);
		camera.mouseControl(mainWindow.getXChange(), mainWindow.getYChange());

		// Clear the window
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shaderList[0].UseShader();
		uniformModel = shaderList[0].GetModelLocation();
		uniformProjection = shaderList[0].GetProjectionLocation();
		uniformView = shaderList[0].GetViewLocation();
		uniformEyePosition = shaderList[0].GetEyePositionLocation();
		uniformSpecularIntensity = shaderList[0].GetSpecularIntensityLocation();
		uniformShininess = shaderList[0].GetShininessLocation();

		shaderList[0].SetDirectionalLight(&mainLight);
		shaderList[0].SetPointLights(pointLights, pointLightCount);

		glUniformMatrix4fv(uniformProjection, 1, GL_FALSE, glm::value_ptr(projection));
		glUniformMatrix4fv(uniformView, 1, GL_FALSE, glm::value_ptr(camera.calculateViewMatrix()));
		glUniform3f(uniformEyePosition, camera.getCameraPosition().x, camera.getCameraPosition().y, camera.getCameraPosition().z);

		glm::mat4 model(1.0f);

		// loop through input
		for (int i = 0; i < elements.size(); ++i) {
			model = glm::mat4(1.0f);
			shinyMaterial.UseMaterial(uniformSpecularIntensity, uniformShininess);
			model = glm::translate(model, glm::vec3(elements[i].get_x() / 1000, -2.0f, elements[i].get_y() / 1000));
			model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
			if (elements[i].get_rotation()) {
				model = glm::rotate(model, glm::float32(toRadians*elements[i].get_rotation()), glm::vec3(0.0f, 1.0f, 0.0f));
			}

			std::string type = elements[i].get_element_type();

			if (type == "Wall")
			{
				model = glm::scale(model, glm::vec3(0.5f*(elements[i].get_height()/100), 0.5f, 1.0f));
				glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
				wall.RenderModel();
			}
			else if (type == "Plant")
			{
				model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
				glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
				plant.RenderModel();
			}
			else if (type == "Armchair" || type == "OfficeChair")
			{
				model = glm::scale(model, glm::vec3(0.004f, 0.004f, 0.004f));
				glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
				chair.RenderModel();
			}
			else if (type == "CornerSofa" || type == "CornerDoubleSofa" || type == "DoubleSofa"
				|| type == "LongSofa" || type == "TriSofa")
			{
				model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.4f));
				model = glm::scale(model, glm::vec3(0.6f, 0.6f, 0.6f));
				glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
				sofa.RenderModel();
			}
			else if (type == "Drawer" || type == "DoubleCloset" || type == "OneSideCloset")
			{
				model = glm::scale(model, glm::vec3(0.07f, 0.07f, 0.07f));
				glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
				dresser.RenderModel();
			}
			else if (type == "Table")
			{
				glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
				table.RenderModel();
			}
			else if (type == "Door")
			{
				model = glm::rotate(model, -90.0f*toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
				model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
				glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
				door.RenderModel();
			}
			else
			{
				model = glm::scale(model, glm::vec3(0.6f, 0.6f, 0.6f));
				brickTexture.UseTexture();
				glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
				meshList[0]->RenderMesh();
			}
		}

		// floor
		model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, -2.0f, 0.0f));
		//model = glm::scale(model, glm::vec3(0.4f, 0.4f, 1.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		floorTexture.UseTexture();
		shinyMaterial.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[2]->RenderMesh();

		glUseProgram(0);

		mainWindow.swapBuffers();
	}
}

Scene::Scene()
{

}

void Scene::launchScene()
{
	launchSceneInner();
}

Scene::~Scene()
{

}