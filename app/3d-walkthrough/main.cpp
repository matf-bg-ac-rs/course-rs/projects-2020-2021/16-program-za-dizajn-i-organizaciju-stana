#define STB_IMAGE_IMPLEMENTATION

#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>

#include "Scene.h"

int main()
{
	Scene scene = Scene();
	scene.launchScene();

	return 0;
}