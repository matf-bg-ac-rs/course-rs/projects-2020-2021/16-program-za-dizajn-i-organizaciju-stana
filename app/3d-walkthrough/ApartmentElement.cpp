#include "ApartmentElement.h"

ApartmentElement::ApartmentElement() {}
ApartmentElement::ApartmentElement(std::string el_type,
	double x_coord,
	double y_coord,
	double el_width,
	double el_length,
	double el_height,
	double rot,
	double el_scale)
{

	element_type = el_type;
	x = x_coord;
	y = y_coord;
	width = el_width;
	length = el_length;
	height = el_height;
	rotation = rot;
	scale = el_scale;
}

std::string ApartmentElement::showElement()
{
	std::string res = "";
	return res.append(element_type)
		.append(": x coord: ").append(std::to_string(x))
		.append(", y coord: ").append(std::to_string(y))
		.append(", width: ").append(std::to_string(width))
		.append(", length: ").append(std::to_string(length))
		.append(", height: ").append(std::to_string(height))
		.append(", rotation: ").append(std::to_string(rotation))
		.append(", scale: ").append(std::to_string(scale))
		.append(", x coord: ").append(std::to_string(x));
}

ApartmentElement::~ApartmentElement() {}