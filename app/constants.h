#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QString>
#include <QPointF>

enum ElementType { lowerCabinetDouble, upperCabinetSingle, upperCabinetDouble,
                   fridge, dishwasher, microwave, aspirator, doubleSink, singleSink,
                   sinkDrain, bathtub, cornerBathtub, bidet, handbasin, cornerHandbasin,
                   roundHandbasin, showerhead, cornerTub, roundTub, squareTub,
                   doubleBed, singleBed, doubleCloset, oneSideCloset, armchair,
                   officeChair, officeDesk, cornerSofa, cornerDoubleSofa, doubleSofa,
                   longSofa, triSofa, aircond, chair, drawer, circle, elipse, wall,
                   door, windowElement};

static QString elementTypeToString(ElementType type){
    switch(type){
        case lowerCabinetDouble: return "LowerCabinetDouble";
        case upperCabinetSingle: return "UpperCabinetSingle";
        case upperCabinetDouble: return "UpperCabinetDouble";
        case fridge: return "Fridge";
        case dishwasher: return "Dishwasher";
        case microwave: return "Microwave";
        case aspirator: return "Aspirator";
        case doubleSink: return "DoubleSink";
        case singleSink: return "SingleSink";
        case sinkDrain: return "SinkDrain";

        case bathtub: return "Bathtub";
        case cornerBathtub: return "CornerBathtub";
        case bidet: return "Bidet";
        case handbasin: return "Handbasin";
        case cornerHandbasin: return "CornerHandbasin";
        case roundHandbasin: return "RoundHandbasin";
        case showerhead: return "Showerhead";
        case cornerTub: return "CornerTub";
        case roundTub: return "RoundTub";
        case squareTub: return "SquareTub";

        case doubleBed: return "DoubleBed";
        case singleBed: return "SingleBed";
        case doubleCloset: return "DoubleCloset";
        case oneSideCloset: return "OneSideCloset";

        case armchair: return "Armchair";
        case officeChair: return "OfficeChair";
        case officeDesk: return "OfficeDesk";
        case cornerSofa: return "CornerSofa";
        case cornerDoubleSofa: return "CornerDoubleSofa";
        case doubleSofa: return "DoubleSofa";
        case longSofa: return "LongSofa";
        case triSofa: return "TriSofa";

        case aircond: return "Aircond";
        case chair: return "Chair";
        case drawer: return "Drawer";
        case circle: return "Circle";
        case elipse: return "Elipse";
        case door: return "Door";
        case windowElement: return "WindowElement";

        default: return "Circle";
    }
}

enum SceneInsertMode {newElement, pastedElement};

const float INIT_HEIGHT = 100.0f;
const float INIT_WIDTH = 100.0f;
const float INIT_DEPTH = 100.0f;

const int LIGHT_INDICATOR = 245;
const int DARK_INDICATOR = 10;

const int INIT_ROOM_HEIGHT = 500;
const int INIT_ROOM_WIDTH = 500;

const int MIN_SIZE = 0;
const int MAX_SIZE = 500;

const float ROTATE_PERCENTAGE = 10.0F;

const QPointF PASTE_OFFSET = QPointF(45, 30);
const QString PIC_EXTENSION = ".png";

const QString WALL_LENGTH_MESSAGE = "Wall length:";

const QString OUTPUT_SEPARATOR = " ";

#endif // CONSTANTS_H
