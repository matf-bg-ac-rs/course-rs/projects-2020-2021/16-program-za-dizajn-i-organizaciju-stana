#ifndef ROOMSCOMPONENT_H
#define ROOMSCOMPONENT_H

#include "tabcontent.h"
#include "elementmodel.h"
#include "properties_component.h"

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class RoomsComponent; }
QT_END_NAMESPACE

class RoomsComponent : public QWidget
{
    Q_OBJECT

public:
    RoomsComponent(QWidget *parent = nullptr, properties_component* pk = nullptr);
    ~RoomsComponent();
    Scene* getCurrScene();

private slots:
    void on_Rooms_tabBarClicked(int index);
    void on_Rooms_tabBarDoubleClicked(int index);
    void on_Rooms_currentChanged(int index);

    void on_deleteRoomBtn_clicked();
    void on_goTo3dBtn_clicked();

    void emitWallLengthHelperSignal(QString text){
        emit wallLengthHelperSignal(text);
    };

public slots:
    void incomingElementSlot(ElementModel *model, SceneInsertMode mode);
    void modeActionGroupClicked(QAction*);
    void funcActionGroupClicked(QAction*);

signals:
    void clearProperties();
    void tabChanging();
    void wallLengthHelperSignal(QString);

private:
    properties_component *pk;
    Ui::RoomsComponent *ui;
    QVector<TabContent*> tabContentByIndex;

    void goTo3D();

    unsigned defaultNewRoomNumber;

    bool isLastTab(int index) const;
    int getNumOfTabs() const;
    int getCurrTabIndex();
    TabContent *getCurrTabContent();

    QString getDefaultTabName();
    QString getValidTabNameFromPopUp(bool& ok);

    void initFirstTab();
    void addTab(QString tabName);
    void addTabWithDefaultName();
    void addTabWithNameFromPopUp();
    bool isRoomDeletionValid();
    QString getFileName(QString location);
    void exportRoomPic();

    void connectElement(ElementModel *el);
    void connectScene(Scene* scene);
};
#endif // ROOMSCOMPONENT_H
