#ifndef PROPERTIES_COMPONENT_H
#define PROPERTIES_COMPONENT_H

#include <QWidget>
#include "elementmodel.h"

QT_BEGIN_NAMESPACE
namespace Ui { class properties_component; }
QT_END_NAMESPACE

class properties_component : public QWidget
{
    Q_OBJECT

public:
    properties_component(QWidget *parent = nullptr);
    ~properties_component();

    void change_lbl_color_show();

private:
    void disableFields(bool disable);
    void clearFields();

public slots:
    void incomingElementSlot(ElementModel *model);
    void nothingSelectedSlot();

private slots:
    void setPositionX(qreal x);
    void setPositionY(qreal y);

    void setSizeW(float width);
    void setSizeH(float height);
    void setSizeD(float depth);
    void setRotation(float z);
    void setName(QString name);

    void on_le_position_x_editingFinished();
    void on_le_position_y_editingFinished();
    void on_le_size_width_editingFinished();
    void on_le_size_height_editingFinished();
    void on_le_size_depth_editingFinished();
    void on_le_rotation_editingFinished();
    void on_le_name_editingFinished();

    void on_btn_color_clicked();
    void on_btn_reset_pixmap_clicked();

private:
    Ui::properties_component *ui;
    ElementModel* currentElement;
};
#endif // PROPERTIES_COMPONENT_H
