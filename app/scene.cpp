#include "scene.h"
#include "constants.h"
#include <mainwindow.h>
#include <iostream>
#include <QRectF>
#include <QPointF>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QVariant>
#include "elementutil.h"

template<typename Base, typename T>
inline bool instanceof(const T*) {
   return std::is_base_of<Base, T>::value;
}

Scene::Scene(QObject* parent): QGraphicsScene(parent)
{
    sceneMode = SelectObject;
    wallElementToDraw = 0;
}

void Scene::setMode(Mode mode){
    sceneMode = mode;
    QGraphicsView::DragMode vMode =
            QGraphicsView::NoDrag;

    if(mode == DrawLine){
        makeItemsControllable(false);
        vMode = QGraphicsView::NoDrag;
    }
    else if(mode == SelectObject){
        makeItemsControllable(true);
        vMode = QGraphicsView::RubberBandDrag;
    }
    else{
        makeItemsControllable(true);
        vMode = QGraphicsView::ScrollHandDrag;
    }

    QGraphicsView* mView = views().at(0);
    if(mView)
        mView->setDragMode(vMode);
}

void Scene::makeItemsControllable(bool areControllable){
    foreach(QGraphicsItem* item, items()){
        item->setFlag(QGraphicsItem::ItemIsSelectable,
                      areControllable);
        item->setFlag(QGraphicsItem::ItemIsMovable,
                      areControllable);
    }
}

void Scene::mousePressEvent(QGraphicsSceneMouseEvent *event){
    if(sceneMode == DrawLine)
        origPoint = event->scenePos();
    QGraphicsScene::mousePressEvent(event);
}

void Scene::mouseMoveEvent(QGraphicsSceneMouseEvent *event){
    if(sceneMode == DrawLine){
        if(!wallElementToDraw){
            wallElementToDraw = new QGraphicsLineItem;
            QGraphicsScene::addItem(wallElementToDraw);
            wallElementToDraw->setPen(QPen(Qt::black, 6, Qt::SolidLine));
            wallElementToDraw->setPos(origPoint);
        }
        wallElementToDraw->setLine(0,0,
                            event->scenePos().x() - origPoint.x(),
                            event->scenePos().y() - origPoint.y());
        auto msg = WALL_LENGTH_MESSAGE + QString::number(wallElementToDraw->line().length());
        emit wallLength(msg);
        wallElementToDraw->setTransformOriginPoint(wallElementToDraw->boundingRect().center());
    }
    else
        QGraphicsScene::mouseMoveEvent(event);
}

void Scene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event){
    wallElementToDraw = 0;
    QGraphicsScene::mouseReleaseEvent(event);
    if(selectedItems().length() != 1){
        emit wallLength("");
        emit clearProperties();
    }
    else{
        dummySelect();
    }
}

void Scene::keyPressEvent(QKeyEvent *event){
    if(event->key() == Qt::Key_Delete)
        deleteSelectedElements();
    else if(event->matches(QKeySequence::Copy)){
        sceneClipboard = selectedItems();
    }
    else if(event->matches(QKeySequence::Paste)){
        clearSelection();
        foreach(auto item, sceneClipboard){
            pasteSceneItem(item);
        }
    }
    else
        QGraphicsScene::keyPressEvent(event);
}

QGraphicsLineItem *Scene::wallCopyConstructor(QGraphicsLineItem *w)
{
    QGraphicsLineItem* newWall = new QGraphicsLineItem;

    newWall->setPen(w->pen());
    newWall->setPos(w->pos() + PASTE_OFFSET);
    newWall->setLine(w->line());
    newWall->setTransformOriginPoint(newWall->boundingRect().center());

    return newWall;
}

void Scene::pasteSceneItem(QGraphicsItem *item)
{
    QGraphicsLineItem* maybeWall = dynamic_cast<QGraphicsLineItem*>( item );
    if(maybeWall){
        auto wallCopy = wallCopyConstructor(maybeWall);
        QGraphicsScene::addItem(wallCopy);
        makeItemsControllable(wallCopy);
        wallCopy->setSelected(true);
        return;
    }
    emit pasteElement((ElementModel*)item, pastedElement);
}

void Scene::dummySelect(){
    if(selectedItems().length()!=1)
        return;

    QGraphicsItem* selectedElement = selectedItems().at(0);

    QGraphicsLineItem* isWall = dynamic_cast<QGraphicsLineItem*>( selectedElement );
    if(isWall){
        auto msg = WALL_LENGTH_MESSAGE + QString::number(((QGraphicsLineItem*)selectedElement)->line().length());
        emit wallLength(msg);
        emit clearProperties();
    }
    else{
        ((ElementModel*)(selectedElement))->dummySelection();
        emit wallLength("");
    }
}



void Scene::rotateElement(float degrees){
    foreach(QGraphicsItem* item, selectedItems()){
        auto angle = item->rotation() + degrees;
        if(angle >= 360.0)
            angle -= 360.0;
        if(angle <= -360.0)
            angle += 360.0;
        item->setRotation(angle);
    }
    dummySelect();
}

void Scene::deleteSelectedElements()
{
    foreach(QGraphicsItem* item, selectedItems()){
        removeItem(item);
        delete item;
    }
    emit clearProperties();
}

void Scene::addElement(QGraphicsItem* item)
{
    QGraphicsScene::addItem(item);
    setMode(Scene::SelectObject);
}
