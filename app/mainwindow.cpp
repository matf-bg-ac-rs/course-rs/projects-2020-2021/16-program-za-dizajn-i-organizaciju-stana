#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "scene.h"
#include <QLabel>
#include <QPushButton>
#include "rooms_component.h"
#include "constants.h"
#include "elementutil.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , propertiesComponent(new properties_component)
    , roomsComponent(new RoomsComponent(this, propertiesComponent))
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setCentralWidget(roomsComponent);
    createToolBar();
    connect(roomsComponent,        &RoomsComponent::tabChanging,
            propertiesComponent,   &properties_component::nothingSelectedSlot,
            Qt::UniqueConnection);


    // properties
    ui->widgetContProperties->setLayout(propertiesComponent->layout());

    vboxKitchen = new QVBoxLayout(ui->widgetContKitchen);
    vboxBathroom = new QVBoxLayout(ui->widgetContBathroom);
    vboxBedroom = new QVBoxLayout(ui->widgetContBedroom);
    vboxLivingRoom = new QVBoxLayout(ui->widgetContLivingRoom);
    vboxGeneral = new QVBoxLayout(ui->widgetContGeneral);

    // location from resources
    initLists();

    // insert widgets in layouts for every group
    initVBox(vboxKitchen, kitchenElemTemplates);
    initVBox(vboxBedroom, bedroomElemTemplates);
    initVBox(vboxBathroom, bathroomElemTemplates);
    initVBox(vboxLivingRoom, livingRoomElemTemplates);
    initVBox(vboxGeneral, generalElemTemplates);

    labelWallLength = new QLabel(ui->mainStatusBar);
    labelWallLength->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
    labelWallLength->resize(300,20);
    QString wallLabelStyle = " QLabel { margin: 0px 0px 6px 10px; }";
    labelWallLength->setStyleSheet(wallLabelStyle);

    connect(roomsComponent, &RoomsComponent::wallLengthHelperSignal,
            this,           &MainWindow::setWallDimensions);
}

void MainWindow::initVBox(QVBoxLayout *vBox, QList<QSharedPointer<ElementModel>> modelList){
    for(int i=0; i < modelList.size(); i++){
        vBox->addWidget(modelList.at(i).get());

        connect(modelList.at(i).get(),  &ElementModel::elementSelected,
                roomsComponent,         &RoomsComponent::incomingElementSlot);
        connect(modelList.at(i).get(),  &QToolButton::clicked,
                this,                   &MainWindow::changeToolbarModeToSelect);
    }
    vBox->addSpacerItem(new QSpacerItem(10,10,QSizePolicy::Expanding, QSizePolicy::Expanding));
}

void MainWindow::changeToolbarModeToSelect()
{
    selectAction->setChecked(true);
}

void MainWindow::setWallDimensions(QString message){
    this->labelWallLength->setText(message);
}

void MainWindow::initLists(){
    kitchenElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateLowerCabinetDouble()));
    kitchenElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateUpperCabinetSingle()));
    kitchenElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateUpperCabinetDouble()));
    kitchenElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateFridge()));
    kitchenElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateDishwasher()));
    kitchenElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateMicrowave()));
    kitchenElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateAspirator()));
    kitchenElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateDoubleSink()));
    kitchenElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateSingleSink()));
    kitchenElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateSinkDrain()));

    bathroomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateBathtub()));
    bathroomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateCornerBathtub()));
    bathroomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateBidet()));
    bathroomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateHandbasin()));
    bathroomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateCornerHandbasin()));
    bathroomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateRoundHandbasin()));
    bathroomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateShowerhead()));
    bathroomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateCornerTub()));
    bathroomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateRoundTub()));
    bathroomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateSquareTub()));

    bedroomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateDoubleBed()));
    bedroomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateSingleBed()));
    bedroomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateDoubleCloset()));
    bedroomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateOneSideCloset()));

    livingRoomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateArmchair()));
    livingRoomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateOfficeChair()));
    livingRoomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateOfficeDesk()));
    livingRoomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateCornerSofa()));
    livingRoomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateCornerDoubleSofa()));
    livingRoomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateDoubleSofa()));
    livingRoomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateLongSofa()));
    livingRoomElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateTriSofa()));

    generalElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateAircond()));
    generalElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateChair()));
    generalElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateDrawer()));
    generalElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateCircle()));
    generalElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateElipse()));
    generalElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateDoor()));
    generalElemTemplates.append(QSharedPointer<ElementModel>(ElementUtil::getTemplateWindowElement()));

}

MainWindow::~MainWindow()
{
    delete labelWallLength;
    delete ui;
    // todo: free mem
}

QAction* MainWindow::createModeAction(QString text, Scene::Mode function, QString icon_path){
    auto action = new QAction(text, this);
    action->setData(int(function));
    action->setIcon(QIcon(icon_path));
    action->setCheckable(true);
    return action;
}

QAction *MainWindow::createFuncAction(QString text, Scene::Functionality function, QString icon_path)
{
    auto action = new QAction(text, this);
    action->setData(int(function));
    action->setIcon(QIcon(icon_path));
    return action;
}

void MainWindow::createActions(){
    dragAction = createModeAction("Drag view", Scene::HandMode, ":/img/img/icons/move.png");
    lineAction = createModeAction("Draw line", Scene::DrawLine, ":/img/img/icons/wall.png");
    selectAction = createModeAction("Select object", Scene::SelectObject, ":/img/img/icons/select.png");


    modeActionGroup = new QActionGroup(this);
    modeActionGroup->setExclusive(true);
    modeActionGroup->addAction(dragAction);
    modeActionGroup->addAction(lineAction);
    modeActionGroup->addAction(selectAction);

    screenshot = createFuncAction("Take a screenshot", Scene::Screenshot, ":/img/img/icons/screenshot.png");
    rotateLeft = createFuncAction("Rotate Left", Scene::RotateLeft, ":/img/img/icons/rotate_left.png");
    rotateLeft90 = createFuncAction("Rotate Left 90 Degrees", Scene::RotateLeft90, ":/img/img/icons/rotate_left_90.png");
    rotateRight = createFuncAction("Rotate Right", Scene::RotateRight, ":/img/img/icons/rotate_right.png");
    rotateRight90 = createFuncAction("Rotate Right 90 Degrees", Scene::RotateRight90, ":/img/img/icons/rotate_right_90.png");
    zoomIn = createFuncAction("Zoom in", Scene::ZoomIn, ":/img/img/icons/zoom_in.png");
    zoomOut = createFuncAction("Zoom out", Scene::ZoomOut, ":/img/img/icons/zoom_out.png");
    deleteSelectedEl = createFuncAction("Delete selected element(s)", Scene::DeleteSelectedEl, ":/img/img/icons/trash can.png");

    funcActionGroup = new QActionGroup(this);
    funcActionGroup->addAction(screenshot);
    funcActionGroup->addAction(rotateLeft);
    funcActionGroup->addAction(rotateLeft90);
    funcActionGroup->addAction(rotateRight);
    funcActionGroup->addAction(rotateRight90);
    funcActionGroup->addAction(zoomIn);
    funcActionGroup->addAction(zoomOut);
    funcActionGroup->addAction(deleteSelectedEl);
    // lambda koja prodje sve ove u listi i pozove f-ju za njih
}

void MainWindow::createToolBar(){

    createActions();

    MainToolBar = new QToolBar;
    addToolBar(Qt::TopToolBarArea, MainToolBar);

    QWidget *spacerWidgetLeft = new QWidget(this);
    spacerWidgetLeft->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Preferred);
    spacerWidgetLeft->setVisible(true);
    QWidget *spacerWidgetRight = new QWidget(this);
    spacerWidgetRight->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Preferred);
    spacerWidgetRight->setVisible(true);

    MainToolBar->addWidget(spacerWidgetLeft);
    MainToolBar->addAction(dragAction);
    MainToolBar->addAction(selectAction);
    MainToolBar->addAction(lineAction);
    MainToolBar->addSeparator();
    MainToolBar->addAction(screenshot);
    MainToolBar->addSeparator();
    MainToolBar->addAction(rotateLeft90);
    MainToolBar->addAction(rotateLeft);
    MainToolBar->addAction(rotateRight);
    MainToolBar->addAction(rotateRight90);
    MainToolBar->addAction(zoomIn);
    MainToolBar->addAction(zoomOut);
    MainToolBar->addAction(deleteSelectedEl);
    MainToolBar->addWidget(spacerWidgetRight);

    changeToolbarModeToSelect();

    connect(modeActionGroup, &QActionGroup::triggered,
            roomsComponent, &RoomsComponent::modeActionGroupClicked);
    connect(funcActionGroup, &QActionGroup::triggered,
            roomsComponent, &RoomsComponent::funcActionGroupClicked);

    connect(roomsComponent, &RoomsComponent::tabChanging,
            this,            &MainWindow::changeToolbarModeToSelect);
}
